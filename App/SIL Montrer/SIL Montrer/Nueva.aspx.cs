﻿using System;

namespace SIL_Montrer
{
    public partial class Nueva : System.Web.UI.Page
    {
        clsInscripcion cInsc = new clsInscripcion ();

        private void fnCatPlanteles ()
        {
            clsCatPlanteles cPlanteles = new clsCatPlanteles ();
            cmbPlantel.DataSource = cPlanteles.fnLista ();

            cmbPlantel.DataTextField = "nombre_plantel";
            cmbPlantel.DataValueField = "id_plantel";
            cmbPlantel.DataBind ();
        }
        private void fnCatModalidades ()
        {
            clsCatModalidades cModalidades = new clsCatModalidades ();
            cmbModalidad.DataSource = cModalidades.fnLista ();

            cmbModalidad.DataTextField = "nombre";
            cmbModalidad.DataValueField = "id";
            cmbModalidad.DataBind ();
        }
        private void fnCatNiveles ()
        {
            clsCatNiveles cNiveles = new clsCatNiveles ();
            cmbNiveles.DataSource = cNiveles.fnLista ();

            cmbNiveles.DataTextField = "nombre";
            cmbNiveles.DataValueField = "id";
            cmbNiveles.DataBind ();
        }
        private void fnCatOfertaEducativa ()
        {
            clsCatOfertaEducativa cOferta = new clsCatOfertaEducativa ();
            cmbOfertaEducativa.DataSource = cOferta.fnLista (cmbPlantel.SelectedValue, cmbNiveles.SelectedValue, cmbModalidad.SelectedValue);

            cmbOfertaEducativa.DataTextField = "nombre";
            cmbOfertaEducativa.DataValueField = "id";
            cmbOfertaEducativa.DataBind ();
        }

        protected void Page_Load (object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                fnCatPlanteles ();
                fnCatModalidades ();
                fnCatNiveles ();
            }
        }

        protected void cmbPlantel_SelectedIndexChanged (object sender, EventArgs e)
        {
            fnCatOfertaEducativa ();
        }

        protected void cmbNiveles_SelectedIndexChanged (object sender, EventArgs e)
        {
            fnCatOfertaEducativa ();
        }

        protected void cmbModalidad_SelectedIndexChanged (object sender, EventArgs e)
        {
            fnCatOfertaEducativa ();
        }

        protected void butEnviar_Click (object sender, EventArgs e)
        {
            var vNombre = Request.Form ["nombre"];
            var vApellido1 = Request.Form ["apellido1"];
            var vApellido2 = Request.Form ["apellido2"];
            var vTelefono = Request.Form ["telefono"];
            var vEmail = Request.Form ["email"];
            var vMatricula = "asd123";

            string sResultado = cInsc.fnAltaPaso1 (vNombre, vApellido1, vApellido2, vTelefono, vEmail, cmbNiveles.SelectedValue,
                cmbPlantel.SelectedValue, null, cmbModalidad.SelectedValue, cmbOfertaEducativa.SelectedValue, cmbPeriodo.SelectedValue, cmbCiclo.SelectedValue,
                vMatricula);

            if (sResultado == "")
            {
                Response.Write ("GUARDADO");
                //Enviar correo
                string sBody = string.Format (new clsMail ().fnBodyRegistroExitoso (), vEmail, vMatricula);
                clsMail cMail = new clsMail (vEmail, "Registro", sBody, "", null);
                cMail.fnEnviarCorreo ();
            }
            else
                Response.Write (sResultado);
        }
    }
}
﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace SIL_Montrer
{
    public class clsDB
    {

        /// <summary>
        /// Obtener conexión a BD
        /// </summary>
        /// <returns></returns>
        private string fnConnStringSQL ()
        {
            return ConfigurationManager.ConnectionStrings ["SILMontrerConnectionString"].ConnectionString;
        }

        /// <summary>
        /// Ejecutar sentencia SELECT
        /// </summary>
        /// <param name="sSentenciaSQL">Sentencia SQL</param>
        /// <returns>Datos obtenidos del Select</returns>
        public DataSet fnSelecSQL (string sSentenciaSQL)
        {
            DataSet ds = new DataSet ();
            SqlDataAdapter da = new SqlDataAdapter ();
            SqlConnection cnn = new SqlConnection ();
            SqlCommand cmd = new SqlCommand ();
            try
            {
                cnn.ConnectionString = fnConnStringSQL ();
                cnn.Open ();
                cmd.CommandText = sSentenciaSQL;
                da.SelectCommand = cmd;
                da.SelectCommand.Connection = cnn;
                da.Fill (ds);
            }
            catch
            {
                return null;
            }
            finally
            {
                cnn.Close ();
            }
            return ds;
        }

        /// <summary>
        /// Ejecuta sentencia SQL (Update, Insert, Delete)
        /// </summary>
        /// <param name="sSentenciaSQL">Sentencia SQL</param>
        /// <returns>"true" Si se ejecutó correctamente; "false" Si ocurrió un error</returns>
        public bool fnEjecutaSQL (string sSentenciaSQL)
        {
            SqlConnection sqlConn = new SqlConnection ();
            SqlCommand sqlCom = new SqlCommand ();
            try
            {
                sqlConn.ConnectionString = fnConnStringSQL ();
                sqlConn.Open ();
                sqlCom.Connection = sqlConn;
                sqlCom.CommandText = sSentenciaSQL;
                sqlCom.ExecuteNonQuery ();
                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                sqlConn.Close ();
            }
        }

        /// <summary>
        /// Ejecuta sentencia SQL (Update, Insert, Delete)
        /// </summary>
        /// <param name="sSentenciaSQL">Sentencia SQL</param>
        /// <returns>"true" Si se ejecutó correctamente; "false" Si ocurrió un error</returns>
        public DataSet fnInsertAndGetIDSQL (string sSentenciaSQL)
        {
            DataSet ds = new DataSet ();
            SqlDataAdapter da = new SqlDataAdapter ();
            SqlConnection cnn = new SqlConnection ();
            SqlCommand cmd = new SqlCommand ();
            try
            {
                cnn.ConnectionString = fnConnStringSQL ();
                cnn.Open ();
                cmd.CommandText = sSentenciaSQL;
                da.SelectCommand = cmd;
                da.SelectCommand.Connection = cnn;
                da.Fill (ds);
            }
            catch (Exception error)
            {
                return null;
            }
            finally
            {
                cnn.Close ();
            }
            return ds;
        }
    }
}
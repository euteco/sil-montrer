﻿using System.Data;

namespace SIL_Montrer
{
    public class clsCatModalidades
    {
        clsDB cDB = new clsDB ();

        /// <summary>
        /// Lista de Modalidades
        /// </summary>
        /// <returns></returns>
        public DataTable fnLista ()
        {
            try
            {
                return cDB.fnSelecSQL (@"
SELECT
    id, nombre
FROM
    [Montrer SIL].dbo.modalidad
WHERE
    activo = 1
ORDER BY
    nombre").Tables [0];
            }
            catch
            {
                return null;
            }
        }
    }
}
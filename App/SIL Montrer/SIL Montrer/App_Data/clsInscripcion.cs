﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SIL_Montrer
{
    public class clsInscripcion
    {
        clsDB cDB = new clsDB ();

        public clsInscripcion ()
        {

        }

        public string fnAltaPaso1 (string sNombre, string sApPaterno, string sApMaterno, string sTelefono, string sMail, string sIdNivel, string sIdPlantel,
            string sIdPlan, string sIdModalidad, string sIdOferta, string sIdPeriodo, string sIdCicloEscolar, string sMatricula)
        {
            //Alta del Alumno
            DataTable dtIdAlumnoNew = cDB.fnInsertAndGetIDSQL (string.Format (@"
                INSERT INTO [Montrer SIL].dbo.alumno 
                    ([nombre], [apellido1], [apellido2], [telefono], [Mail], Matricula) 
                VALUES 
                    ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}');
                SELECT SCOPE_IDENTITY();",
                sNombre, sApPaterno, sApMaterno, sTelefono, sMail, sMatricula)).Tables [0];
            if (dtIdAlumnoNew != null)
            {
                //Alta de la Inscripcion
                if (cDB.fnEjecutaSQL (string.Format (@"
                    INSERT INTO [Montrer SIL].dbo.inscripcion
                        ([idAlumno], [idPeriodo], [idNivel], [idOferta], [idPlantel], [idModalidad], [idCiclo])
                    VALUES
                        ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')",
                        dtIdAlumnoNew.Rows [0] [0].ToString (), sIdPeriodo, sIdNivel, sIdOferta, sIdPlantel, sIdModalidad, sIdCicloEscolar)))
                {
                    return "";
                }
                else
                    return "Error al registrar la inscripción del alumno";
            }
            else
                return "Error al registrar al alumno";
        }

        public bool fnValidarMail (string sID)
        {
            //Proceso para validar
            //Si todo bien
            if (sID == "OK")
                return true;
            else
                return false;
        }
    }
}
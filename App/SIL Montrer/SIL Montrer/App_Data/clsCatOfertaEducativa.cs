﻿using System.Data;

namespace SIL_Montrer
{
    public class clsCatOfertaEducativa
    {
        clsDB cDB = new clsDB ();

        /// <summary>
        /// Lista de Ofertas Educativas por Plantel, Nivel y Modalidad
        /// </summary>
        /// <returns></returns>
        public DataTable fnLista (string sIdPlantel, string sIdNivel, string sIdModalidad)
        {
            try
            {
                return cDB.fnSelecSQL (
                    string.Format (@"
SELECT 
    tOf.id, tOf.nombre
FROM 
    [Montrer SIL].dbo.oferta AS tOf
	INNER JOIN [Montrer SIL].[dbo].[oferta_plantel] AS tP ON tOf.id = tP.id_oferta_educativa
WHERE
    tOf.activo = 1
    AND tP.id_plantel = {0}
	AND tOf.idNivel = {1}
	AND tOf.idModalidad = {2}	
ORDER BY 
    tOf.nombre", sIdPlantel, sIdNivel, sIdModalidad)).Tables [0];
            }
            catch
            {
                return null;
            }
        }
    }
}
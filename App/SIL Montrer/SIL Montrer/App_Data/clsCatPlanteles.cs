﻿using System.Data;

namespace SIL_Montrer
{
    public class clsCatPlanteles
    {
        clsDB cDB = new clsDB ();

        /// <summary>
        /// Lista de Planteles
        /// </summary>
        /// <returns></returns>
        public DataTable fnLista ()
        {
            try
            {
                return cDB.fnSelecSQL (@"
SELECT
    id_plantel, nombre_plantel
FROM
    [Montrer SIL].dbo.plantel
WHERE
    activo = 1
ORDER BY
    nombre_plantel").Tables [0];
            }
            catch
            {
                return null;
            }
        }
    }
}
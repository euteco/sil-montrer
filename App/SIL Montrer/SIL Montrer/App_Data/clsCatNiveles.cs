﻿using System.Data;

namespace SIL_Montrer
{
    public class clsCatNiveles
    {
        clsDB cDB = new clsDB ();

        /// <summary>
        /// Lista de Niveles
        /// </summary>
        /// <returns></returns>
        public DataTable fnLista ()
        {
            try
            {
                return cDB.fnSelecSQL (@"
SELECT
    id, nombre
FROM
    [Montrer SIL].dbo.nivel
WHERE
    activo = 1
ORDER BY
    nombre").Tables [0];
            }
            catch
            {
                return null;
            }
        }
    }
}
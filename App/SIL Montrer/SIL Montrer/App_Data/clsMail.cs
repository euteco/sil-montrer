﻿using System;
using System.Data;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace SIL_Montrer
{
    public class clsMail
    {
        string sDe, sPara, sNombre, sAsunto, sMensaje, sAdjunto, sPass, sSMTP, sCopia;
        Attachment attArchivoAdjunto;
        string [] sParaN = new string [0];
        int iPuerto;
        bool bSSL;

        clsDB cDatos = new clsDB ();

        private DataTable fnConfigCorreo ()
        {
            return cDatos.fnSelecSQL ("SELECT * FROM [Montrer SIL].[dbo].tInfoCorreoEvaluaciones").Tables [0];
        }

        public clsMail ()
        {

        }

        /// <summary>
        /// clase para envío de correos
        /// </summary>
        /// <param name="sPara1">Correo Destino</param>
        /// <param name="sNombre1">Nombre Remitente</param>
        /// <param name="sAsunto1"></param>
        /// <param name="sMensaje1"></param>
        /// <param name="sAdjunto1">Ruta del Archivo Adjunto</param>
        /// <param name="attAdjunto">Insertar archivo adjunto directamente</param>
        public clsMail (string sPara1, string sAsunto1, string sMensaje1, string sAdjunto1, Attachment attAdjunto)
        {
            sPara = sPara1;
            sAsunto = sAsunto1;
            sMensaje = sMensaje1;
            sAdjunto = sAdjunto1;
            attArchivoAdjunto = attAdjunto;

            DataTable dtConfigMail = fnConfigCorreo ();
            if (dtConfigMail != null && dtConfigMail.Rows.Count > 0)
            {
                if (sPara1 == "")
                    sPara = dtConfigMail.Rows [0] ["Correo"].ToString ();
                sNombre = dtConfigMail.Rows [0] ["Nombre"].ToString ();
                sDe = dtConfigMail.Rows [0] ["Correo"].ToString ();
                sPass = dtConfigMail.Rows [0] ["Pass"].ToString ();
                iPuerto = Convert.ToInt16 (dtConfigMail.Rows [0] ["Puerto"].ToString ());
                sSMTP = dtConfigMail.Rows [0] ["SMTP"].ToString ();
                bSSL = Convert.ToBoolean (dtConfigMail.Rows [0] ["sSSL"]);
            }
        }

        /// <summary>
        /// clase para envío de correos con copia
        /// </summary>
        /// <param name="sPara1">Correo Destino</param>
        /// <param name="sCopia1">Correo con Copia</param>
        /// <param name="sNombre1">Nombre Remitente</param>
        /// <param name="sAsunto1"></param>
        /// <param name="sMensaje1"></param>
        /// <param name="sAdjunto1">Ruta del Archivo Adjunto</param>
        /// <param name="attAdjunto">Insertar archivo adjunto directamente</param>
        public clsMail (string sPara1, string sCopia1, string sAsunto1, string sMensaje1, string sAdjunto1, Attachment attAdjunto)
        {
            sPara = sPara1;
            sCopia = sCopia1;
            sAsunto = sAsunto1;
            sMensaje = sMensaje1;
            sAdjunto = sAdjunto1;
            attArchivoAdjunto = attAdjunto;

            DataTable dtConfigMail = fnConfigCorreo ();
            if (dtConfigMail != null && dtConfigMail.Rows.Count > 0)
            {
                if (sPara1 == "")
                    sPara = dtConfigMail.Rows [0] ["Correo"].ToString ();
                sNombre = dtConfigMail.Rows [0] ["Nombre"].ToString ();
                sDe = dtConfigMail.Rows [0] ["Correo"].ToString ();
                sPass = dtConfigMail.Rows [0] ["Pass"].ToString ();
                iPuerto = Convert.ToInt16 (dtConfigMail.Rows [0] ["Puerto"].ToString ());
                sSMTP = dtConfigMail.Rows [0] ["SMTP"].ToString ();
                bSSL = Convert.ToBoolean (dtConfigMail.Rows [0] ["sSSL"]);
            }
        }

        /// <summary>
        /// clase para envío de correos
        /// </summary>
        /// <param name="sPara1">Colección de Correos Destino</param>
        /// <param name="sAsunto1"></param>
        /// <param name="sMensaje1"></param>
        /// <param name="sAdjunto1">Ruta del Archivo Adjunto</param>
        /// <param name="attAdjunto">Insertar archivo adjunto directamente</param>
        public clsMail (string [] sPara1, string sAsunto1, string sMensaje1, string sAdjunto1, Attachment attAdjunto)
        {
            if (sPara1.Length > 0)
            {
                foreach (string sDestino in sPara1)
                {
                    Array.Resize (ref sParaN, sParaN.Length + 1);
                    sParaN [sParaN.Length - 1] = sDestino;
                }
            }
            sAsunto = sAsunto1;
            sMensaje = sMensaje1;
            sAdjunto = sAdjunto1;
            attArchivoAdjunto = attAdjunto;

            DataTable dtConfigMail = fnConfigCorreo ();
            if (dtConfigMail != null && dtConfigMail.Rows.Count > 0)
            {
                sNombre = dtConfigMail.Rows [0] ["Nombre"].ToString ();
                sDe = dtConfigMail.Rows [0] ["Correo"].ToString ();
                sPass = dtConfigMail.Rows [0] ["Pass"].ToString ();
                iPuerto = Convert.ToInt16 (dtConfigMail.Rows [0] ["Puerto"].ToString ());
                sSMTP = dtConfigMail.Rows [0] ["SMTP"].ToString ();
                bSSL = Convert.ToBoolean (dtConfigMail.Rows [0] ["sSSL"]);
            }
        }

        /// <summary>
        /// Consulta de parámetros predeterminados para el envío de correos
        /// </summary>
        /// <returns></returns>
        public string clsMailAsunto ()
        {
            DataTable dtConfigMail = fnConfigCorreo ();
            if (dtConfigMail != null && dtConfigMail.Rows.Count > 0)
                return dtConfigMail.Rows [0] ["Asunto"].ToString ();
            return "";
        }

        /// <summary>
        /// Enviar correo
        /// </summary>
        /// <returns>'' si se envía el correo, ERROR cuando no se envía el correo</returns>
        public string fnEnviarCorreo ()
        {
            if (sDe == string.Empty)
                return "No se ha configurado el correo de envío";

            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage ();
            if (sPara != "")
                msg.To.Add (sPara);
            else
            {
                foreach (string sDestino in sParaN)
                    msg.To.Add (sDestino);
            }
            if (!string.IsNullOrEmpty (sCopia))
                msg.CC.Add (sCopia);
            msg.From = new MailAddress (sDe, sNombre, System.Text.Encoding.UTF8);
            msg.Subject = sAsunto;
            msg.SubjectEncoding = System.Text.Encoding.UTF8;
            msg.Body = sMensaje;
            msg.BodyEncoding = System.Text.Encoding.UTF8;
            msg.IsBodyHtml = true;
            if (sAdjunto != "")
            {
                String sFile = sAdjunto;
                Attachment oAttch = new Attachment (sFile);
                msg.Attachments.Add (oAttch);
            }
            if (attArchivoAdjunto != null)
                msg.Attachments.Add (attArchivoAdjunto);

            SmtpClient client = new SmtpClient ();
            client.Credentials = new System.Net.NetworkCredential (sDe, sPass);// cSeguridad.Decrypt (sPass));
            client.Port = iPuerto;
            client.Host = sSMTP;
            client.EnableSsl = bSSL;
            try
            {
                client.Send (msg);//Enviamos el mensaje
                return "";
            }
            catch (System.Net.Mail.SmtpException error)
            {
                return error.Message;
            }
            finally
            {
                msg.Dispose ();
            }
        }

        /// <summary>
        /// Enviar correo con N cantidad de adjuntos
        /// </summary>
        /// <param name="sAdjuntos"></param>
        /// <returns>'' si se envía el correo, ERROR cuando no se envía el correo</returns>
        public string fnEnviarCorreo (string [] sAdjuntos)
        {
            if (sDe == string.Empty)
                return "No se ha configurado el correo de envío";

            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage ();
            if (sPara != "")
                msg.To.Add (sPara);
            else
            {
                foreach (string sDestino in sParaN)
                    msg.To.Add (sDestino);
            }
            if (!string.IsNullOrEmpty (sCopia))
                msg.CC.Add (sCopia);
            msg.From = new MailAddress (sDe, sNombre, System.Text.Encoding.UTF8);
            msg.Subject = sAsunto;
            msg.SubjectEncoding = System.Text.Encoding.UTF8;
            msg.Body = sMensaje;
            msg.BodyEncoding = System.Text.Encoding.UTF8;
            msg.IsBodyHtml = true;
            if (sAdjuntos.Length > 0)
            {
                foreach (string sCadaAdjunto in sAdjuntos)
                {
                    String sFile = sCadaAdjunto;
                    Attachment oAttch = new Attachment (sFile);
                    msg.Attachments.Add (oAttch);
                }
            }
            if (attArchivoAdjunto != null)
                msg.Attachments.Add (attArchivoAdjunto);

            SmtpClient client = new SmtpClient ();
            client.Credentials = new System.Net.NetworkCredential (sDe, sPass);// cSeguridad.Decrypt (sPass));
            client.Port = iPuerto;
            client.Host = sSMTP;
            client.EnableSsl = bSSL;
            try
            {
                client.Send (msg);//Enviamos el mensaje
                return "";
            }
            catch (System.Net.Mail.SmtpException error)
            {
                return error.Message;
            }
            finally
            {
                msg.Dispose ();
            }
        }

        /// <summary>
        /// Generar string del html para enviarlo en el cuerpo del mensaje
        /// </summary>
        /// <returns>html en string</returns>
        public string fnBodyRegistroExitoso ()
        {
            using (WebClient wc = new WebClient ())
            {
                wc.Encoding = Encoding.UTF8;
                string [] sSecciones = HttpContext.Current.Request.Url.Segments;
                return wc.DownloadString (HttpContext.Current.Request.Url.AbsoluteUri.Replace (sSecciones [sSecciones.Length - 1], "ConfirmaMailRegistro.aspx"));
            }
        }
    }
}
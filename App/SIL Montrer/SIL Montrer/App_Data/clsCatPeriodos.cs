﻿using System;
using System.Data;


namespace SIL_Montrer
{
    public class clsCatPeriodos
    {
        public DataTable fnListaPorOferta (string sIdOferta)
        {
            clsDB cDB = new clsDB ();

            try
            {
                return cDB.fnSelecSQL (string.Format (@"
SELECT 
    tPe.id_Periodo, tOf.numPeriodos, tPe.[nombre_periodo] 
FROM 
    [Montrer SIL].dbo.oferta AS tOf 
	INNER JOIN [Montrer SIL].dbo.periodo AS tPe ON tOf.idPeriodo = tPe.id_Periodo
WHERE 
    tOf.id = {0}", sIdOferta)).Tables [0];
            }
            catch
            {
                return null;
            }
        }

        public string [] fnListaPorOfertaEnumerado (string sIdOferta)
        {
            clsDB cDB = new clsDB ();

            try
            {
                DataTable dtPeriodos = cDB.fnSelecSQL (string.Format (@"
SELECT 
    tPe.id_Periodo, tOf.numPeriodos, tPe.[nombre_periodo] 
FROM 
    [Montrer SIL].dbo.oferta AS tOf 
	INNER JOIN [Montrer SIL].dbo.periodo AS tPe ON tOf.idPeriodo = tPe.id_Periodo
WHERE 
    tOf.id = {0}", sIdOferta)).Tables [0];

                if (dtPeriodos != null && dtPeriodos.Rows.Count > 0)
                    return fnsPeriodosEnumerados (dtPeriodos.Rows [0] ["nombre_periodo"].ToString (),
                        Convert.ToInt16 (dtPeriodos.Rows [0] ["numPeriodos"]));
                return null;
            }
            catch
            {
                return null;
            }
        }

        private string [] fnsPeriodosEnumerados (string sPeriodo, int iCantidad)
        {
            string [] sLista = new string [iCantidad];
            for (int iRecorre = 0; iRecorre < iCantidad; iRecorre++)
            {
                if (iRecorre == 0)
                    sLista [iRecorre] = string.Format ("{0}er {1}", (iRecorre + 1).ToString (), sPeriodo);
                else
                    sLista [iRecorre] = string.Format ("{0}do {1}", (iRecorre + 1).ToString (), sPeriodo);
            }
            return sLista;
        }
    }
}
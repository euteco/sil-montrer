﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Pruebas.aspx.cs" Inherits="SIL_Montrer.Pruebas" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <h3>Registro correcto</h3>
            <p>Has realizado el primer paso en tu registro</p>
            <p>
                Puedes continuar tu proceso con los siguientes datos:            
            </p>
            <div class="row">
                <div class="col-sm-2">
                    Usuario:
                </div>
                <div class="col-sm-9">{0}</div>
            </div>
            <div class="row">
                <div class="col-sm-2">
                    Matrícula:
                </div>
                <div class="col-sm-9">{1}</div>
            </div>
        </div>
    </form>
</body>
</html>

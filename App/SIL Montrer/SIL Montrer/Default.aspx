﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SIL_Montrer.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Sistema de inscripciones en línea Montrier" />
    <meta name="keywords" content="universidad, prepa, maestría, doctorado, morelia, en linea, michoacán, méxico, Montrer" />
    <meta name="author" content="EUTECO" />

    <title>SIL principal</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/inicial.css" />
</head>

<body>
    <form id="form1" runat="server" class="needs-validation formulario">
        
    </form>
        <!-- Menú incio --------------------------------------------------------------------------->
        <header class="transparent">
            <div class="row">
                <div class="col-md-12">
                    <!-- logo inicio -->
                    <div id="logo">
                        <a href="Default.aspx">
                            <img class="logo" src="images/logo.png" alt="Logo universidad montrer" />
                        </a>
                    </div>
                    <!-- logo fin -->

                    <!-- small button inicio -->
                    <span id="menu-btn"></span>
                    <!-- small button fin -->

                    <!-- mainmenu inicio -->
                    <nav>
                        <ul id="mainmenu">
                            <li><a href="Default.aspx" class="active">Inicio</a>
                            </li>
                            <li><a href="Nueva.aspx">Nueva inscripción</a>
                            </li>
                            <li><a href="#continuar" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample">Continuar proceso </a>
                            </li>
                            <li><a href="#reinscripcion" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample">Reinscripción </a>

                            </li>

                        </ul>
                    </nav>

                </div>
                <!-- mainmenu fin -->

            </div>
        </header>

        <!-- Menú fin --------------------------------------------------------------------------->


        <!-- inicio sesión --------------------------------------------------------------------------->
        <div class="collapse" id="reinscripcion">
            <div class="card card-body">
                <h3>Reinscripción</h3>
                <form class="needs-validation formulario" novalidate action="#" method="post">
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="validationCustomUsername">E-mail</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroupPrepend"><i class="far fa-envelope"></i></span>
                                </div>
                                <input type="email" class="form-control" id="validationCustomUsername" placeholder="yo@mimail.com" aria-describedby="inputGroupPrepend" required />
                                <div class="invalid-feedback">
                                    No puede dejar este campo en blanco, verifique que su email esté bien escrito
				
                                </div>
                                <div class="valid-feedback">
                                    Bien
				
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="validationCustom02">Matricula</label>
                            <input type="text" class="form-control" id="matricula" placeholder="" value="" required />
                            <div class="invalid-feedback">
                                No puede dejar este campo en blanco
				
                            </div>
                            <div class="valid-feedback">
                                Bien
				 
                            </div>
                        </div>

                    </div>
                    <button class="btn btn-primary" type="submit">Entrar</button>
                    <button class="btn btn-primary" data-target="#reinscripcion" aria-controls="reinscripcion" aria-expanded="false" data-toggle="collapse" type="button">Cancelar</button>

                </form>
            </div>

        </div>

        <!-- Fin sesión --------------------------------------------------------------------------->



        <!-- inicio sesión --------------------------------------------------------------------------->
        <div class="collapse" id="continuar">
            <div class="card card-body">
                <h3>Continuar con el proceso de mi inscripción</h3>
                <form class="needs-validation formulario" novalidate action="validamatricula.php" method="post">
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="validationCustomUsername">E-mail</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroupPrepend"><i class="far fa-envelope"></i></span>
                                </div>
                                <input type="emailc" class="form-control" id="validationCustomUsername" placeholder="yo@mimail.com" aria-describedby="inputGroupPrepend" required />
                                <div class="invalid-feedback">
                                    No puede dejar este campo en blanco, verifique que su email esté bien escrito
				
                                </div>
                                <div class="valid-feedback">
                                    Bien
				
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="validationCustom02">Matricula</label>
                            <input type="text" class="form-control" id="matriculac" placeholder="" value="" required />
                            <div class="invalid-feedback">
                                No puede dejar este campo en blanco
				
                            </div>
                            <div class="valid-feedback">
                                Bien
				 
                            </div>
                        </div>

                    </div>
                    <button class="btn btn-primary" type="submit">Entrar</button>
                    <button class="btn btn-primary" data-target="#continuar" aria-controls="continuar" aria-expanded="false" data-toggle="collapse" type="button">Cancelar</button>
                </form>
            </div>

        </div>

        <!-- Fin sesión --------------------------------------------------------------------------->

        <div class="area1">
            <h1 class="centrado resalteverde">Sistema de Inscripciones en Línea Montrer</h1>
        </div>
        <div class="area2"></div>
        <div class="area3"></div>
        <!--<div class="area4"></div>


<div class="area5"></div>
<div class="area6">area 6</div>
<div class="area7">area 7</div>
<div class="area8">area 8</div>
<div class="area9">area 9</div>
-->


        <!-- INICIO FOOTER -->
        <footer id="footer">
            <div id="footer-main" class="container-large">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="footext">
                            <h3>Dudas o aclaraciones </h3>


                            <p><i class="far fa-envelope"></i><a href="mailto:serviciosescolares@unimontrer.edu.mx">serviciosescolares@unimontrer.edu.mx</a></p>
                            <p><i class="fas fa-phone"></i><a class="fblanca" href="tel:(+52 443) 324-04-39" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Llámanos">(+52 443) 324-04-39</a></p>
                            <p><i class="fas fa-phone-volume"></i><a class="fblanca" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Extención">Extención: 187</a></p>

                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="footext">

                            <img width="200" height="50" class="footer-logo" src="images/logo.png" alt="logo" />
                        </div>
                    </div>

                </div>
            </div>
            <div id="footer-bottom">
                <div class="cr">
                    <div class="row">
                        <div class="col-sm-12 t-center">
                            <p class="copyright fblanca">© EUTECO. All Rights Reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- FIN FOOTER -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html>

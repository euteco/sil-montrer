﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Nueva.aspx.cs" Inherits="SIL_Montrer.Nueva" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>SIL Nueva</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Sistema de inscripciones en línea Montrier" />
    <meta name="keywords" content="universidad, prepa, maestría, doctorado, morelia, en linea, michoacán, méxico, Montrer" />
    <meta name="author" content="EUTECO" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/inicial.css" />
    <link rel="stylesheet" href="build/css/intlTelInput.css" />
    <link rel="stylesheet" href="build/css/demo.css" />
</head>
<body>
    <form id="form1" runat="server" class="needs-validation formulario" novalidate>
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
        <!-- Menú incio --------------------------------------------------------------------------->
        <header class="transparent">

            <div class="row">
                <div class="col-md-12">
                    <!-- logo inicio -->
                    <div id="logo">
                        <a href="Default.aspx">
                            <img class="logo" src="images/logo.png" alt="Logo universidad montrer" />
                        </a>
                    </div>
                    <!-- logo fin -->

                    <!-- small button inicio -->
                    <span id="menu-btn"></span>
                    <!-- small button fin -->

                    <!-- mainmenu inicio -->
                    <nav>
                        <ul id="mainmenu">
                            <li><a href="Default.aspx" class="active">Inicio</a>

                            </li>


                        </ul>
                    </nav>

                </div>
                <!-- mainmenu fin -->

            </div>
        </header>

        <!-- Menú fin --------------------------------------------------------------------------->
        <div class="row">
            <div class="fondo1 col-0 col-md-4"></div>
            <div class="contenedor col-12 col-md-8">
                <h1>Nueva inscripción</h1>
                <%--<form class="needs-validation formulario" novalidate>--%>
                <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <label for="validationCustom01">Nombre</label>
                        <%--<asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" required></asp:TextBox>--%>
                        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="" value="" required />
                        <div class="invalid-feedback">
                            No puede dejar este campo en blanco
                        </div>
                        <div class="valid-feedback">
                            Bien
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="validationCustom02">Primer apellido</label>
                        <%--<asp:TextBox ID="txtapellido1" runat="server" CssClass="form-control" required></asp:TextBox>--%>
                        <input type="text" class="form-control" id="apellido1" name="apellido1" placeholder="" value="" required />
                        <div class="invalid-feedback">
                            No puede dejar este campo en blanco
                        </div>
                        <div class="valid-feedback">
                            Bien
                        </div>
                    </div>

                </div>
                <div class="form-row">


                    <div class="col-md-6 mb-3">
                        <label for="validationCustom02">Segundo apellido</label>
                        <%--<asp:TextBox ID="txtapellido2" runat="server" CssClass="form-control" required></asp:TextBox>--%>
                        <input type="text" class="form-control" id="apellido2" name="apellido2" placeholder="" value="" required />
                        <div class="invalid-feedback">
                            No puede dejar este campo en blanco
                        </div>
                        <div class="valid-feedback">
                            Bien
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="validationCustomUsername">E-mail</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroupPrepend"><i class="far fa-envelope"></i></span>
                            </div>
                            <%--<asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" required placeholder="yo@mimail.com" 
                                aria-describedby="inputGroupPrepend" TextMode="Email"></asp:TextBox>--%>
                            <input type="email" class="form-control" id="email" name="email" placeholder="yo@mimail.com" aria-describedby="inputGroupPrepend" required />
                            <div class="invalid-feedback">
                                No puede dejar este campo en blanco, verifique que su email esté bien escrito
                            </div>
                            <div class="valid-feedback">
                                Este mail tendrás que verificarlo para llevar a cabo el próximo paso del proceso
                            </div>
                        </div>
                    </div>


                </div>


                <div class="form-row">


                    <div class="col-md-6 mb-3">
                        <label for="validationCustom02">Telefono de casa o celular</label>
                        <!-- id diferente a nombre por flojera  -->
                        <%--<asp:TextBox ID="phone" runat="server" CssClass="form-control" required placeholder="0000-000-000"></asp:TextBox>--%>
                        <input id="phone" name="telefono" type="tel" placeholder="0000-000-000" required />
                        <div class="invalid-feedback">
                            No puede dejar este campo en blanco
                        </div>
                        <div class="valid-feedback">
                            Bien
                        </div>


                    </div>




                </div>
                <div class="agrupacion">

                    <asp:UpdatePanel ID="updOferta" runat="server">
                        <ContentTemplate>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label for="validationCustom02">Plantel</label>
                                        <asp:DropDownList ID="cmbPlantel" runat="server" CssClass="custom-select" required AutoPostBack="True" OnSelectedIndexChanged="cmbPlantel_SelectedIndexChanged"></asp:DropDownList>
                                        <div class="invalid-feedback">No puede dejar este campo en blanco</div>
                                    </div>

                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label for="validationCustom02">Nivel</label>
                                        <asp:DropDownList ID="cmbNiveles" runat="server" CssClass="custom-select" required AutoPostBack="True" OnSelectedIndexChanged="cmbNiveles_SelectedIndexChanged"></asp:DropDownList>
                                        <div class="invalid-feedback">No puede dejar este campo en blanco</div>
                                    </div>

                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label for="validationCustom02">Modalidad</label>
                                        <asp:DropDownList ID="cmbModalidad" runat="server" CssClass="custom-select" required AutoPostBack="True" OnSelectedIndexChanged="cmbModalidad_SelectedIndexChanged"></asp:DropDownList>
                                        <div class="invalid-feedback">No puede dejar este campo en blanco</div>
                                    </div>

                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label for="validationCustom02">Oferta Educativa</label>
                                        <asp:DropDownList ID="cmbOfertaEducativa" runat="server" CssClass="custom-select" required></asp:DropDownList>
                                        <div class="invalid-feedback">No puede dejar este campo en blanco</div>
                                    </div>

                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <div class="form-group">
                                <label for="validationCustom02">Periodo al que ingresará</label>
                                <asp:DropDownList ID="cmbPeriodo" runat="server" CssClass="custom-select" reqired>
                                    <asp:ListItem Text="1"></asp:ListItem>
                                    <asp:ListItem Text="2"></asp:ListItem>
                                    <asp:ListItem Text="3"></asp:ListItem>
                                    <asp:ListItem Text="4"></asp:ListItem>
                                    <asp:ListItem Text="5"></asp:ListItem>
                                    <asp:ListItem Text="6"></asp:ListItem>
                                    <asp:ListItem Text="7"></asp:ListItem>
                                    <asp:ListItem Text="8"></asp:ListItem>
                                </asp:DropDownList>
                                <%--<select id="periodo" name="periodo" class="custom-select" required>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                </select>--%>
                                <div class="invalid-feedback">No puede dejar este campo en blanco</div>
                            </div>

                        </div>
                        <div class="col-md-6 mb-3">
                            <div class="form-group">
                                <label for="validationCustom02">Ciclo Escolar al que se inscribe</label>
                                <asp:DropDownList ID="cmbCiclo" runat="server" CssClass="custom-select" reqired>
                                    <asp:ListItem Text="1"></asp:ListItem>
                                    <asp:ListItem Text="2"></asp:ListItem>
                                    <asp:ListItem Text="3"></asp:ListItem>
                                    <asp:ListItem Text="4"></asp:ListItem>
                                    <asp:ListItem Text="5"></asp:ListItem>
                                    <asp:ListItem Text="6"></asp:ListItem>
                                    <asp:ListItem Text="7"></asp:ListItem>
                                    <asp:ListItem Text="8"></asp:ListItem>
                                </asp:DropDownList>
                                <%--<select id="ciclo" name="ciclo" class="custom-select" required>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                </select>--%>
                                <div class="invalid-feedback">No puede dejar este campo en blanco</div>
                            </div>

                        </div>
                    </div>
                </div>

                <asp:Button ID="butEnviar" runat="server" CssClass="btn btn-primary" OnClick="butEnviar_Click" Text="Enviar" />
                <%--<button class="btn btn-primary" type="button">Enviar</button>--%>
                <%--</form>--%>
            </div>


        </div>

        <!-- INICIO FOOTER -->
        <footer id="footer">
            <div id="footer-main" class="container-large">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="footext">
                            <h3>Dudas o aclaraciones </h3>


                            <p><i class="far fa-envelope"></i><a href="mailto:serviciosescolares@unimontrer.edu.mx">serviciosescolares@unimontrer.edu.mx</a></p>
                            <p><i class="fas fa-phone"></i><a class="fblanca" href="tel:(+52 443) 324-04-39" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Llámanos">(+52 443) 324-04-39</a></p>
                            <p><i class="fas fa-phone-volume"></i><a class="fblanca" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Extención">Extención: 187</a></p>

                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="footext">

                            <img width="200" height="50" class="footer-logo" src="images/logo.png" alt="logo">
                        </div>
                    </div>

                </div>
            </div>
            <div id="footer-bottom">
                <div class="cr">
                    <div class="row">
                        <div class="col-sm-12 t-center">
                            <p class="copyright fblanca">© EUTECO. All Rights Reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- FIN FOOTER -->



        <script>
            // Example starter JavaScript for disabling form submissions if there are invalid fields
            (function () {
                'use strict';
                window.addEventListener('load', function () {
                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                    var forms = document.getElementsByClassName('needs-validation');
                    // Loop over them and prevent submission
                    var validation = Array.prototype.filter.call(forms, function (form) {
                        form.addEventListener('submit', function (event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();
        </script>


        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
        <!-- Load jQuery from CDN so can run demo immediately -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="build/js/intlTelInput.js"></script>
        <script>
            $("#phone").intlTelInput({
                // allowDropdown: false,
                // autoHideDialCode: false,
                // autoPlaceholder: "off",
                // dropdownContainer: "body",
                // excludeCountries: ["us"],
                // formatOnDisplay: false,
                geoIpLookup: function (callback) {
                    $.get("http://ipinfo.io", function () { }, "jsonp").always(function (resp) {
                        var countryCode = (resp && resp.country) ? resp.country : "";
                        callback(countryCode);
                    });
                },
                // hiddenInput: "full_number",
                initialCountry: "auto",
                // nationalMode: false,
                // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
                placeholderNumberType: "Móvil",
                preferredCountries: ['mx', 'co', 'gu'],
                // separateDialCode: true,
                utilsScript: "build/js/utils.js"
            });
        </script>

    </form>
</body>
</html>

﻿using System;

namespace SIL_Montrer.Pantallas_Auxiliares
{
    public partial class ValidarMailNuevaInscripcion : System.Web.UI.Page
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            if (Request.QueryString ["ID"] != null &&Request.QueryString["ID"]!= "" )
            {
                clsInscripcion cIns = new clsInscripcion ();
                if (cIns.fnValidarMail (Request.QueryString ["ID"]))
                    Response.Redirect ("Paso 2.aspx");
                else
                {
                    //Mensaje de error;
                }
            }
        }
    }
}
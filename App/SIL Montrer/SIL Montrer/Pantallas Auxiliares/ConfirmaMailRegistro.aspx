﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConfirmaMailRegistro.aspx.cs" Inherits="SIL_Montrer.ConfirmaMailRegistro" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="es-mx">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Confirmación de correo</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <h3>Registro correcto</h3>
            <p>Has realizado el primer paso en tu registro</p>
            <p>
                Puedes continuar tu proceso con los siguientes datos:            
            </p>
            <div class="row">
                <div class="col-sm-2">
                    Usuario:
                </div>
                <div class="col-sm-9">{0}</div>
            </div>
            <div class="row">
                <div class="col-sm-2">
                    Matrícula:
                </div>
                <div class="col-sm-9">{1}</div>
            </div>
            <div class="row">
                <a href="#" class="btn-link">Aquí va el link para continuar el proceso</a>
            </div>
        </div>
    </form>
</body>
</html>
